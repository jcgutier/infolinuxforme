# infolinuxforme

This is an example app that counts the number of files: audio(mp3), video(mp4), and images(jpg) on S3.

The infrastructure is deployed on AWS and managed with Terraform. 

It uses 3 ASG one for `/audio` one for `video` and one for `imagenes`. A ALB is used and 3 Target Groups for each endpoint.

The app is built with Flask and runs with Gunicorn server. It is contenerized with Docker and image is uploaded to ECR.

In this example can be found the roles and policies needed to access ECR and S3 from EC2.

This app also uses the Gitlab CI/CD to get every change on the application deployed at the moment to running EC2 instances.

## Infrastructure Deploy

1. terraform apply -target aws_key_pair.infolinux
2. terraform apply

## Application Deploy
This could be built and deployed with GitLab CI/CD or with [build_push.sh](scripts/build_push.sh) and [deploy.sh](scripts/deploy.sh)
