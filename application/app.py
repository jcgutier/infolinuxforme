#!/usr/bin/env python

from flask import Flask, render_template, request
import subprocess
import os

app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    ip_address = request.remote_addr
    print(ip_address)
    return render_template("index.html", hostname=os.uname()[1])

@app.route('/audio')
@app.route('/video')
@app.route('/imagenes')
def audio():
    ip_address = request.remote_addr
    print(f"Request comes from {ip_address}")
    rule = request.url_rule
    rule = str(rule)
    if "audio" in rule:
        command = "./s3files.sh | grep mp3 | wc -l"
    elif "video" in rule:
        command = "./s3files.sh | grep mp4 | wc -l"
    elif "imagenes" in rule:
        command = "./s3files.sh | grep jpg | wc -l"
    ps = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    output = output.decode('utf-8')
    title = rule.strip("/").upper()
    return render_template("template.html", title=title, filenumber=output.strip(), hostname=os.uname()[1])
