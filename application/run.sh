#!/bin/sh

source venv/bin/activate

WORKERS=$(($(nproc) * 2 + 1))
gunicorn app:app -b :5000 --workers=$WORKERS --worker-class=gevent --worker-connections=1000 --timeout 90
