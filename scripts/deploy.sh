#!/bin/bash

set -ex

cd ..
ECR_URL="$(aws ecr describe-repositories --repository-name infolinuxforme | grep repositoryUri | cut -d ':' -f 2 | tr -d '", ')"
EC2PUBIP=`aws ec2 describe-instances --filters "Name=tag:Name,Values=infolinuxforme" | grep \"PublicIpAddress\" | cut -d ':' -f 2 | tr -d '\", '}`
for host in $EC2PUBIP; do echo $host;ssh -o "StrictHostKeyChecking=no" -i terraform/temp.pem ubuntu@$host "docker ps | grep myapp && docker stop myapp;docker rm myapp || echo \"myapp container not found\"";done
for host in $EC2PUBIP; do echo $host;ssh -o "StrictHostKeyChecking=no" -i terraform/temp.pem ubuntu@$host "eval `aws ecr get-login --no-include-email` && docker pull \"$ECR_URL:latest\" && docker run -d -p 80:5000 --name myapp \"$ECR_URL:latest\"";done
