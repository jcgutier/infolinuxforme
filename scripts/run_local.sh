#!/bin/bash

set -ex

cd ..
docker rmi `docker images | grep infolinuxforme | awk '{print $3}'` || echo "No se encontraron imagenes para remover"
rm -rf application/__pycache__
docker build -t flask/hello-world .
ECR_URL="$(aws ecr describe-repositories --repository-name infolinuxforme | grep repositoryUri | cut -d ':' -f 2 | tr -d '", ')"
docker tag "flask/hello-world:latest" "$ECR_URL:latest"
docker rmi flask/hello-world:latest
docker run -d --rm -p 5000:5000 --name myapp "$ECR_URL:latest"
