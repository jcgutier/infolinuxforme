#!/bin/bash

set -ex

cd ..
docker stop myapp && docker rm myapp || echo "No running container"
docker rmi `docker images | grep infolinuxforme | awk '{print $3}'`
rm -rf application/__pycache__
docker build -t flask/hello-world .
ECR_URL="$(aws ecr describe-repositories --repository-name infolinuxforme | grep repositoryUri | cut -d ':' -f 2 | tr -d '", ')"
docker tag "flask/hello-world:latest" "$ECR_URL:latest"
eval `aws ecr get-login --no-include-email`
docker push "$ECR_URL:latest"
docker rmi flask/hello-world:latest
