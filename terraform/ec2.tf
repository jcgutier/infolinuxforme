resource "aws_launch_template" "infolinuxforme" {
  image_id      = var.ami
  instance_type = "t2.micro"

  iam_instance_profile {
    name = aws_iam_instance_profile.infolinuxforme.name
  }

  key_name               = var.key_name
  vpc_security_group_ids = ["${aws_security_group.infolinuxforme.id}"]

  user_data = base64encode("${data.template_file.script.rendered}")
}

resource "aws_autoscaling_group" "infolinuxforme" {
  name               = "infolinuxforme"
  availability_zones = data.aws_availability_zones.available.names
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  tag {
    key                 = "Name"
    value               = "infolinuxforme"
    propagate_at_launch = true
  }

  launch_template {
    id      = aws_launch_template.infolinuxforme.id
    version = "$Latest"
  }
  target_group_arns = ["${aws_lb_target_group.infolinuxformeTG.arn}"]
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "template_file" "script" {
  template = "${file("${path.module}/init.sh")}"
  vars = {
    aws_region = var.region
  }
}

resource "aws_key_pair" "infolinux" {
  key_name   = var.key_name
  public_key = tls_private_key.infolinuxforme.public_key_openssh
}

resource "tls_private_key" "infolinuxforme" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "foo" {
  content         = tls_private_key.infolinuxforme.private_key_pem
  filename        = "temp.pem"
  file_permission = "0400"
}

## ASG 2 ##
resource "aws_autoscaling_group" "infolinuxforme2" {
  name               = "infolinuxforme2"
  availability_zones = data.aws_availability_zones.available.names
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  tag {
    key                 = "Name"
    value               = "infolinuxforme"
    propagate_at_launch = true
  }

  launch_template {
    id      = aws_launch_template.infolinuxforme.id
    version = "$Latest"
  }
  target_group_arns = ["${aws_lb_target_group.infolinuxformeTG2.arn}"]
}

## ASG 3 ##
resource "aws_autoscaling_group" "infolinuxforme3" {
  name               = "infolinuxforme3"
  availability_zones = data.aws_availability_zones.available.names
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  tag {
    key                 = "Name"
    value               = "infolinuxforme"
    propagate_at_launch = true
  }

  launch_template {
    id      = aws_launch_template.infolinuxforme.id
    version = "$Latest"
  }
  target_group_arns = ["${aws_lb_target_group.infolinuxformeTG3.arn}"]
}
