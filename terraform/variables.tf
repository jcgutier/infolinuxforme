variable "region" {
  type = string
}

variable "ami" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "key_name" {
  type = string
}

variable "gitlab_project_id" {
  type = number
}
