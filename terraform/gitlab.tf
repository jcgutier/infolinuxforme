resource "gitlab_project_variable" "sample_project_variable" {
  project           = data.gitlab_project.infolinuxforme.id
  key               = "SSH_PRIVATE_KEY"
  value             = tls_private_key.infolinuxforme.private_key_pem
  environment_scope = "*"
}

data "gitlab_project" "infolinuxforme" {
  id = var.gitlab_project_id
}

resource "gitlab_project_variable" "ecr_url" {
  project           = data.gitlab_project.infolinuxforme.id
  key               = "ECR_URL"
  value             = aws_ecr_repository.infolinuxforme.repository_url
  environment_scope = "*"
}

data "external" "pass" {

  program = ["bash", "${path.module}/getpass.sh"]

}

resource "gitlab_project_variable" "ecr_pass" {
  project           = data.gitlab_project.infolinuxforme.id
  key               = "ECR_PSS"
  value             = data.external.pass.result["password"]
  environment_scope = "*"
}

resource "gitlab_project_variable" "awskeyid" {
  project           = data.gitlab_project.infolinuxforme.id
  key               = "AWS_ACCESS_KEY_ID"
  value             = data.external.pass.result["awskeyid"]
  environment_scope = "*"
}

resource "gitlab_project_variable" "awskeypass" {
  project           = data.gitlab_project.infolinuxforme.id
  key               = "AWS_SECRET_ACCESS_KEY"
  value             = data.external.pass.result["awskeypass"]
  environment_scope = "*"
}

resource "gitlab_project_variable" "awsdregion" {
  project           = data.gitlab_project.infolinuxforme.id
  key               = "AWS_DEFAULT_REGION"
  value             = var.region
  environment_scope = "*"
}
