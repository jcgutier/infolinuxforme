#!/bin/bash

set -ex

apt update
curl -fsSL https://get.docker.com/ | sh
systemctl start docker
systemctl enable docker
grep docker /etc/group || groupadd docker
usermod -aG docker ubuntu
apt install python-pip -y
pip install pip --upgrade
pip install setuptools --upgrade
pip install awscli
echo "export PATH=~/.local/bin/:$PATH" >> ~ubuntu/.bashrc
echo "export AWS_DEFAULT_REGION=${aws_region}" >> ~ubuntu/.bashrc
export AWS_DEFAULT_REGION=${aws_region}
eval `aws ecr get-login --no-include-email`
ECR_URL="$(aws ecr describe-repositories --repository-name infolinuxforme | grep repositoryUri | cut -d ':' -f 2 | tr -d '", ')"
aws ecr list-images --repository infolinuxforme | grep imageTag | grep latest\
  && docker pull "$ECR_URL:latest"\
  && docker run -d -p 80:5000 --name myapp "$ECR_URL:latest"\
  || echo "No hay imagen para descargar"