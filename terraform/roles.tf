resource "aws_iam_role" "infolinuxforme" {
  name               = "infolinuxforme"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "infolinuxforme" {
  name   = "infolinuxforme"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "",
        "Effect": "Allow",
        "Action": [
          "ecr:DescribeImages",
          "ecr:DescribeRepositories",
          "ecr:GetAuthorizationToken",
          "ecr:BatchGetImage",
          "ecr:GetDownloadUrlForLayer",
          "ecr:ListImages"
         ],
          "Resource": ["*"]
      }
    ]
}
EOF
}

resource "aws_iam_policy" "infolinuxformeS3" {
  name   = "infolinuxformeS3"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": ["s3:ListBucket"],
      "Resource": ["${aws_s3_bucket.infolinuxforme.arn}"]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "infolinuxforme" {
  role       = aws_iam_role.infolinuxforme.name
  policy_arn = aws_iam_policy.infolinuxforme.arn
}

resource "aws_iam_role_policy_attachment" "infolinuxformeS3" {
  role       = aws_iam_role.infolinuxforme.name
  policy_arn = aws_iam_policy.infolinuxformeS3.arn
}

resource "aws_iam_instance_profile" "infolinuxforme" {
  name = "infolinuxforme"
  role = aws_iam_role.infolinuxforme.name
}
