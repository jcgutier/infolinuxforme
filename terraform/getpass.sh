#!/bin/bash
set -e
pass="$(aws ecr get-login-password)"
echo "{\"password\":\"$pass\", \
       \"awskeyid\":\"$AWS_ACCESS_KEY_ID\", \
       \"awskeypass\":\"$AWS_SECRET_ACCESS_KEY\"}"
