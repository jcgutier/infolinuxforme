locals {
  interval = 60
  timeout = 20
}


resource "aws_lb" "infolinuxforme" {
  name               = "infolinuxforme"
  subnets            = data.aws_subnet_ids.selected.ids
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.infolinuxforme.id}"]
}

resource "aws_lb_target_group" "infolinuxformeTG" {
  name        = "infolinuxformeTG"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "instance"

  health_check {
    interval            = local.interval
    path                = "/index"
    port                = 80
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = local.timeout
    protocol            = "HTTP"
  }
}

resource "aws_lb_listener" "infolinuxforme" {
  load_balancer_arn = aws_lb.infolinuxforme.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.infolinuxformeTG.arn
    type             = "forward"
  }
}

resource "aws_lb_listener_rule" "static" {
  listener_arn = aws_lb_listener.infolinuxforme.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.infolinuxformeTG.arn
  }

  condition {
    path_pattern {
      values = ["/audio"]
    }
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = var.vpc_id
}

## ASG 2 ##
resource "aws_lb_target_group" "infolinuxformeTG2" {
  name        = "infolinuxformeTG2"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "instance"

  health_check {
    interval            = local.interval
    path                = "/index"
    port                = 80
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = local.timeout
    protocol            = "HTTP"
  }
}

resource "aws_lb_listener_rule" "static2" {
  listener_arn = aws_lb_listener.infolinuxforme.arn
  priority     = 200

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.infolinuxformeTG2.arn
  }

  condition {
    path_pattern {
      values = ["/video"]
    }
  }
}

## ASG 3 ##
resource "aws_lb_target_group" "infolinuxformeTG3" {
  name        = "infolinuxformeTG3"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "instance"

  health_check {
    interval            = local.interval
    path                = "/index"
    port                = 80
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = local.timeout
    protocol            = "HTTP"
  }
}

resource "aws_lb_listener_rule" "static3" {
  listener_arn = aws_lb_listener.infolinuxforme.arn
  priority     = 300

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.infolinuxformeTG3.arn
  }

  condition {
    path_pattern {
      values = ["/imagenes"]
    }
  }
}
