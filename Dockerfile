FROM python:3.6-alpine

COPY ./application/ application

WORKDIR /application

RUN apk add gcc musl-dev libffi-dev make
RUN python -m venv venv \
    && venv/bin/pip install pip --upgrade \
    && venv/bin/pip install setuptools --upgrade \
    && venv/bin/pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["./run.sh"]
